import yt
import sys
import numpy as np
from multiprocessing import Pool,current_process,cpu_count
import parse_vela_metadata


whichVELA = sys.argv[1]
a0s = np.arange(.200,.500,.05)
T = True
F = False
metal_plot = F
phase_plot_allspace = F
phase_plot_allspace_in_Rvir = F
phase_plot_withinpoint1Rvir = F
projectionplot= F
phase_plot_outsidepoint1Rvir = F
phase_plot_outsidepoint5Rvir = F
outflow_phase = T
inflow_phase = T

start = "~/quasarlines/phaseplots"

Rdict = parse_vela_metadata.dict_of_vela_info("Rvir",[int(whichVELA[-2:])],a0s)

def make_plots(a0):
	a0str = "%1.3f"%a0
	filename = '~/scratch/10MpcBox_csf512_a0.'+a0str[2:]+'.d'
	z = 1/a0 -1
	try:
		ds = yt.load(filename)
		Rvir = float(Rdict[whichVELA][a0str])
	except:
		return
	m,center = ds.find_max("density")
	if phase_plot_allspace:
		plot1 = yt.PhasePlot(ds.all_data(), "density", "temperature", ["cell_mass"], weight_field = None)
		plot1.set_xlim(10**-33,10**-19)
		plot1.set_ylim(10**2, 10**8)
		plot1.save(start+"phase_plot_allspace_in_Rvir/"+whichVELA+"_z"+str(z)[:5]+"PhasePlot.png")		
	if phase_plot_allspace_in_Rvir:
		sp = ds.sphere(center, (Rvir, "kpc"))
		plot1 = yt.PhasePlot(sp, "density", "temperature", ["cell_mass"], weight_field = None)
		plot1.set_xlim(10**-33,10**-19)
		plot1.set_ylim(10**2, 10**8)
		plot1.save(start+"phase_plot_allspace_in_Rvir/"+whichVELA+"_z"+str(z)[:5]+"PhasePlot.png")
	if phase_plot_withinpoint1Rvir:
		sp = ds.sphere(center, (.1*Rvir, "kpc"))
		plot2 = yt.PhasePlot(sp, "density", "temperature", ["cell_mass"], weight_field = None)
		plot2.set_xlim(10**-33,10**-19)
		plot2.set_ylim(10**2, 10**8)
		plot2.save(start+"phase_plot_withinpoint1Rvir/"+whichVELA+"_z"+str(z)[:5]+"PhasePlot_within.1Rvir.png")
	if projectionplot:
		plot3 = yt.ProjectionPlot(ds,2,"density",center,width = (2.5*Rvir,"kpc"))
		plot3.save(start+"projectionplot/"+whichVELA+"_z"+str(z)[:5]+"ProjectionPlot.png")
	if metal_plot:
		plot1 = yt.PhasePlot(ds.all_data(), "density", "temperature", ["metal_mass_fraction"], weight_field = None)
		plot1.set_xlim(10**-33,10**-19)
		plot1.set_ylim(10**2, 10**8)
		plot1.save(start+"MetalPhasePlot/"+whichVELA+"_z"+str(z)[:5]+"MetalPhasePlot.png")
	if phase_plot_outsidepoint1Rvir:
		sp1 = ds.sphere(center, (Rvir, "kpc"))
		sp2 = ds.sphere(center, (.1*Rvir, "kpc"))
		shell = sp1-sp2
		plot2 = yt.PhasePlot(shell, "density", "temperature", ["cell_mass"], weight_field = None)
		plot2.set_xlim(10**-33,10**-19)
		plot2.set_ylim(10**2, 10**8)
		plot2.save(start+"phase_plot_outsidepoint1Rvir/"+whichVELA+"_z"+str(z)[:5]+"phase_plot_outsidepoint1Rvir.png")
	if phase_plot_outsidepoint5Rvir:
		sp1 = ds.sphere(center, (Rvir, "kpc"))
		sp2 = ds.sphere(center, (.5*Rvir, "kpc"))
		shell = sp1-sp2
		plot2 = yt.PhasePlot(shell, "density", "temperature", ["cell_mass"], weight_field = None)
		plot2.set_xlim(10**-33,10**-19)
		plot2.set_ylim(10**2, 10**8)
		plot2.save(start+"phase_plot_outsidepoint5Rvir/"+whichVELA+"_z"+str(z)[:5]+"phase_plot_outsidepoint5Rvir.png")
	if inflow_phase:
		sp1 = ds.sphere(center, (Rvir, "kpc"))
		sp1_out = sp1.cut_region('obj["radial_velocity"] < 0')
		sp2 = ds.sphere(center, (.1*Rvir, "kpc"))
		shell = sp1_out-sp2
		plot5 = yt.PhasePlot(shell, "density", "temperature", ["cell_mass"], weight_field = None)
		plot5.set_xlim(10**-33,10**-19)
		plot5.set_ylim(10**2, 10**8)
		plot5.save(start+"inflow_phase/"+whichVELA+"_z"+str(z)[:5]+"inflow_phase.png")
	if outflow_phase:
		sp1 = ds.sphere(center, (Rvir, "kpc"))
		sp1_out = sp1.cut_region('obj["radial_velocity"] > 0')
		sp2 = ds.sphere(center, (.1*Rvir, "kpc"))
		shell = sp1_out-sp2
		plot5 = yt.PhasePlot(shell, "density", "temperature", ["cell_mass"], weight_field = None)
		plot5.set_xlim(10**-33,10**-19)
		plot5.set_ylim(10**2, 10**8)
		plot5.save(start+"outflow_phase/"+whichVELA+"_z"+str(z)[:5]+"outflow_phase.png")

pool = Pool(processes = len(a0s))
pool.map(make_plots,a0s)










