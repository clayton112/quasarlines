import numpy as np
import trident
import yt
import os
import sys
from multiprocessing import Pool,current_process,cpu_count
import itertools
import parse_vela_metadata
from sys import platform as _platform

#yt.funcs.mylog.setLevel(25)

def convert_to_xyz(r, theta, phi):
    return np.array([r*np.sin(theta)*np.cos(phi),r*np.sin(theta)*np.cos(phi),r*np.cos(theta)])

def ray_endpoints_spherical(R,r,theta,phi,alpha):
    start = convert_to_xyz(R,theta,phi)
    xhat = convert_to_xyz(1,np.pi/2-theta,np.pi+phi)
    yhat = convert_to_xyz(1,np.pi/2,3/4*np.pi+phi)
    mid = r*(np.cos(alpha)*xhat+np.sin(alpha)*yhat)
    t = np.linalg.norm((start,mid))/(2*R)
    end = start*(1-t)+mid*t
    return np.array([start,end])

def ion_to_field_name(ion):
    atom = ion.split(" ")[0]
    ionization = trident.roman.from_roman(ion.split(" ")[1])-1
    return "%s_p%s_number_density"%(atom,ionization)

class QuasarSphere(object):
    def __init__(self,ions,a0,sim_name=None,dspath=None,data = None,params = None,Rvir = None):
        if params == None:
            try:
                self.params = [None]*15
                self.params[0] = sim_name
                self.params[12] = 0
                self.params[13] = dspath
                self.params[14] = Rvir
            except:
                print("You need to supply sim_name and dspath!")
                self.params = None
            if dspath:
                self.ds = yt.load(dspath)
                self.params[1] = convert_a0_to_redshift(a0)
            else:
                self.ds = None
                self.params[1] = 0.0
        else:
            self.params = params
            self.ds = yt.load(params[-1])
        if type(ions) is list:
            self.ions = ions
        else:
            self.ions = ions[1:-1].split(",")

        self.info = data

    def _angular_points(self,n,dx,is_r,starting_guess,largest_r):
        i=0
        if is_r:
            x1 = np.linspace(0,largest_r,n)
        else:
            x1 = np.linspace(0,np.pi/2,n)
        points = np.zeros((starting_guess,2))
        for x in x1:
            if x == 0.0:
                y1 = np.zeros(1)
            else:
                if is_r:
                    y1 = np.arange(0,2*np.pi,dx/(2*np.pi*x))+np.random.uniform()*2*np.pi
                else:
                    y1 = np.arange(0,2*np.pi,dx/(2*np.pi*self.params[2]*np.sin(x)))+np.random.uniform()*2*np.pi
            for y in y1:
                points[i] = np.array([x,y])
                i+=1
        return points[:i]
    
    def _retry_with_input(self,params,center,Rvir):
        newR,newlat_n,newr_n = float(params[0]),int(params[1]),int(params[2])
        newlong_dx,newalpha_dx = float(params[3]),float(params[4])
        newlargest_r = float(params[5])
        if len(params)>6:
            newlength = int(params[6].split('=')[-1])
        else:
            newlength = None
        return self.create_QSO_endpoints(newR,newlat_n,newr_n,newlong_dx,\
                                    newalpha_dx,newlargest_r,length = newlength,\
                                    center = center,forsure = False,Rvir = Rvir,overwrite=True)

    def create_QSO_endpoints(self,R,lat_n,r_n,long_dx,alpha_dx, largest_r, center = None, \
        length = None,distances = "kpc",starting_guess = 5000000, forsure = True, overwrite = False):
        if distances == "kpc":
            convert = self.ds.length_unit.in_units('kpc').value
            R /= convert
            long_dx /= convert
            alpha_dx /= convert
            largest_r /= convert
        if not overwrite and self.info is not None:
            cont = raw_input("Overwrite QuasarSphere Object info? (0 for no)  ")
            if cont == 0:
                return
        if center is None:
            if self.ds:
                center = self.ds.find_max(("gas","density"))[1].in_units("kpc").value/convert
                self.sp = self.ds.sphere(center,(4*Rvir,"kpc"))
            else:
                center = np.zeros(3)
        self.params[2:7] = R,lat_n,r_n,long_dx,alpha_dx
        self.params[7:10] = center
        self.params[10] = largest_r
        
        starts_tp = self._angular_points(lat_n,long_dx,False,starting_guess,largest_r)
        mids_ra = self._angular_points(r_n,alpha_dx,True,starting_guess,largest_r)

        self.info = np.zeros((len(starts_tp)*len(mids_ra),12+3*len(self.ions)+1))-1.0
        i=0
        rs = np.linspace(0,largest_r,r_n)
        for start in starts_tp:
            theta = start[0]
            phi = start[1]
            for mid in mids_ra:
                r = mid[0]
                alpha = mid[1]
                self.info[i][0] = i
                self.info[i][1:6] = np.array([R,theta,phi,r,alpha])
                self.info[i][6:9] = ray_endpoints_spherical(R,r,theta,phi,alpha)[0] + center
                self.info[i][9:12] = ray_endpoints_spherical(R,r,theta,phi,alpha)[1] + center 
                i+=1
                if length and i >= length:
                    self.info = self.info[:length]
                    break
        if not length:
            length = len(self.info)
        self.params[11] = length
        print(str(length)+" LOSs to scan.")
        if not forsure:
            go = raw_input("Is this an appropriate number? (0 for no) \n")
            if go == 0:
                newparams = raw_input("Please enter new parameters in the form: (Rvir = %s)\n"%params[14]+\
                    "[R,lat_n,r_n,long_dx,alpha_dx,largest_r,length = None]\n")
                return self._retry_with_input(newparams,center,Rvir)
        return length



    def get_coldens(self, save = 10, parallel = False):
        tosave = save
        starting_point = self.params[12]
        if not parallel:
            for vector in self.info[starting_point:]:
                self.params[12]+=1
                print("%s/%s"%(self.params[12],len(self.info)))
                vector = _get_coldens_helper((self.ds,self.params,vector,self.ions))
                tosave -= 1
                if tosave == 0:
                    output = self.save_values()
                    print("file saved to "+output+".")
                    tosave = save
        if parallel:
            bins = np.append(np.arange(0,self.params[11],save),self.params[11])
            pool = Pool(processes = save,maxtasksperchild = 3)
            for i in range(0, len(bins)-1):
                current_info = self.info[bins[i]:bins[i+1]]
                if current_info[-1,0] < starting_point:
                    continue
                print("%s-%s /%s"%(bins[i],bins[i+1],len(self.info)))
                new_info = pool.map(_get_coldens_helper,itertools.izip(itertools.repeat(self.ds),itertools.repeat(self.params),current_info, itertools.repeat(self.ions)))
                self.info[bins[i]:bins[i+1]] = new_info
                self.params[12]+=save
                output = self.save_values()
                print("file saved to "+output+".")
        output = self.save_values()
        print("file saved to "+output+".")
        return self.info
    
    def save_values(self,dest = None):
        print(self.params)
        if len(self.info[0]) <= 12:
            print("No ions!")
        linesfinished = self.params[12]
        numlines = self.params[11]
        redshift = self.params[1]
        simname = self.params[0]
        ionsstr = ""
        for ion in self.ions:
            ionsstr += "_"+ion.replace(" ","")
        if dest:
            filename = dest
        else:
            foldername = "output/"+simname+"coldensinfo"
            if not os.path.exists(foldername):
                os.mkdir(foldername)
            specificfilename = "%s_of_%s-"%(str(linesfinished),str(numlines)) +ionsstr+"_z"+str(redshift)[:4]+".txt"
            filename = foldername+"/"+specificfilename
            prev = os.listdir(foldername)
            for item in prev:
                if item.endswith("of_%s-"%str(linesfinished) +ionsstr+"_z"+str(redshift)[:4]+".txt"):
                    os.remove(foldername+"/"+item)
        f = file(filename,"w+")
        firstline = "[dsname, z, R, lat_n, r_n, long_dx, alpha_dx, center[0], center[1], center[2], largest_r, totallines, numfinished, pathname]\n"
        secondline = str(self.params)+"\n"
        thirdline = "[i, R, theta, phi, r, alpha, x1, y1, z1, x2, y2, z2"
        for ion in self.ions:
            thirdline += ", "+ion
        f.write(firstline)
        f.write(secondline)
        f.write(thirdline+"]\n")
        for vector in self.info:
            f.write(str(vector).replace("\n",""))
            f.write("\n")
        f.close()
        return filename

def read_values(filename):
    """ firstline = "[dsname, z, R, lat_n, r_n, long_dx, alpha_dx, center[0], center[1], center[2], largest_r, totallines, numfinished, dspath]\n"
        secondline = str(self.params)+"\n"
        thirdline = "[i, R, theta, phi, r, alpha, x1, y1, z1, x2, y2, z2"
    """
    f = file(filename)
    firstline = f.readline()
    secondline = f.readline()[:-1]
    thirdline = f.readline()[:-2].split(", ")
    params = eval(secondline)
    length= params[-3]
    ions = thirdline[12:]
    data = np.zeros((length,len(thirdline)))
    for i in range(length):
        data[i] = np.fromstring(f.readline()[1:-1],sep = " ")
    return params,ions,data

def _get_coldens_helper(dsparamsvectorions):
    ds = dsparamsvectorions[0]
    params = dsparamsvectorions[1]
    vector = dsparamsvectorions[2]
    ions = dsparamsvectorions[3]
    print(str(current_process()))
    ident = str(current_process()).split(",")[0][-2:]
    start = vector[6:9]
    end = vector[9:12]
    if True:
        ray = trident.make_simple_ray(ds,
                    start_position=start,
                    end_position=end,
                    data_filename="ray"+ident+".h5",
                    lines=ions,
                    ftype='gas')
        trident.add_ion_fields(ray, ions)
        field_data = ray.sphere(params[7:10],4*params[14])
        for i in range(len(ions)):
            ion = ions[i]
            cdens = np.sum(field_data[ion_to_field_name(ion)] * field_data['dl'])
            outcdens = np.sum((field_data['radial_velocity']>0)*field_data[ion_to_field_name(ion)]*field_data['dl'])
            incdens = np.sum((field_data['radial_velocity']<0)*field_data[ion_to_field_name(ion)]*field_data['dl'])
            vector[12+3*i] = cdens
            vector[12+3*i+1] = outcdens
            vector[12+3*i+2] = incdens
        Z = np.average(field_data["metallicity"],weights=field_data['dl'])
        vector[-1] = Z
        if _platform == 'darwin':
            foldername = "/Users/claytonstrawn/Desktop/astroresearch/code/ready_for_pleiades/quasarlines"
        else:
            foldername = "/u/cstrawn/quasarlines/galaxy_catalogs/"
        try:
            os.remove(foldername+"/"+"ray"+ident+".h5")
        except:
            pass
    else:
        for i in range(len(ions)):
            ion = ions[i]
            cdens = -2.0
            vector[12+i] = cdens    
    print("vector = "+str(vector))
    return vector


#R,lat_n,r_n,long_dx,alpha_dx, center = None, largest_r = None,length = None,distances = "kpc",starting_guess = 50000):
#    def create_QSO_endpoints(self,R,lat_n,r_n,long_dx,alpha_dx, largest_r, center = None, \

def convert_a0_to_redshift(a0):
    return 1.0/float(a0)-1

if __name__ == "__main__":
    simname = sys.argv[1]
    filename = sys.argv[2]
    ions = sys.argv[3][1:-1].split(",")

    parallel = True
    from sys import platform as _platform
    if _platform == "darwin":
        save = 5
    else:
        save = cpu_count()-2
    forsure=True
    print(save)
    a0 = float(filename[-6:-2])
    Rvir = parse_vela_metadata.get_one_value("Rvir",int(simname[-2:]),a0)
    foldername = simname+"coldensinfo"
    toread = None
    overwrite = True
    if os.path.exists(foldername) and not overwrite:
        ionsstr = ""
        for ion in ions:
            ionsstr += "_"+ion.replace(" ","")
        prev = os.listdir(foldername)
        redshift = convert_a0_to_redshift(a0)
        for item in prev:
            print(item)
            print(ionsstr+"_z"+str(redshift)[:4]+".txt")
            if item.endswith(ionsstr+"_z"+str(redshift)[:4]+".txt"):
                toread = foldername + "/" + item
        if toread:
            params, _, data = read_values(toread)
            q = QuasarSphere(ions,a0,data = data,params = params)
    if not os.path.exists(foldername) or not toread:
        q = QuasarSphere(ions,a0,sim_name = simname,dspath = filename,Rvir = Rvir)
        q.create_QSO_endpoints(2*Rvir, 5, 8, 3*Rvir, Rvir,1.1*Rvir,starting_guess = 100000000,forsure = forsure)
    q.get_coldens(save = save,parallel = parallel)




















