#!/bin/bash

#from VELA08 to VELA15
#10MpcBox_csf512_a0.020.d 
#from a0.330 to a0.500

SOURCEDIR=/lou/s2l/dceverin/VELA_v2.0
#SOURCEDIR=testdir
i=$1
a=$2
IONS=$3



#module load python/2.7.12
#virtualenv --system-site-packages ~/myenv
source ~/myenv/bin/activate
scp lou:$SOURCEDIR/VELA$i/'10MpcBox_csf512_a0.'$a'.d' ~/scratch
scp lou:$SOURCEDIR/VELA$i/'PMcrs0a0.'$a'.DAT' ~/scratch
scp lou:$SOURCEDIR/VELA$i/'PMcrda0.'$a'.DAT' ~/scratch
python ~/quasarlines/quasarlines/one_phase_plot.py 'VELA_v2_'$i '~/scratch/10MpcBox_csf512_a0.'$a'.d'
deactivate 
cd ~/scratch
rm *.d
rm *.DAT
rm *.h5
cd ~/quasarlines/quasarlines/bashstuff
