def get_a0(z):
    return [1.0/(1+z[0]),1.0/(1+z[1])]
import numpy as np

quantity_dict_single = {"a":0,"Rvir":1,"Rdisk":2,"Mvir":3,\
                "gas_Rvir":4,"star_Rvir":5,"dm_Rvir":6,\
                "gas_.1Rvir":5,"star_.1Rvir":6,"dm_.1Rvir":7,\
                "gas_10kpc":8,"star_10kpc":9,"dm_10kpc":10,\
                "gas_Rdisk":11,"star_Rdisk":12,"dm_Rdisk":13}
quantity_dict_triple = {"L":[8,9,10],"cm":[2,3,4],"vcm":[5,6,7]}

def dict_of_vela_info(quantity, iarr = np.arange(6,16), a0s = np.arange(.200,.500,.05), zends = None, a0step = .05):
    if quantity in quantity_dict_single.keys():
        index = quantity_dict_single[quantity]
        numvals = 1
    elif quantity in quantity_dict_triple.keys():
        index = quantity_dict_triple[quantity]
        numvals = 3        
    ret_dict = {}
    basepath = "galaxy_catalogs/"
    if zends:
        a0step
        a0s = np.arange(get_a0(zends)[0],get_a0(zends)[1],a0step)
    for i in range(len(iarr)):
        folder = "VELA_v2_%02i"%iarr[i]
        ret_dict[folder] = {}
        if numvals == 1:
            pathname = basepath + folder + "/galaxy_catalogue/Mstar.txt"
        else:
            pathname = basepath + folder + "/galaxy_catalogue/Nir_disc_cat.txt"
        try:
            f = file(pathname)
        except:
            print("Error reading %s"%pathname)
            continue
        f.readline()
        a = "0"
        while float(a) < a0s[0]:
            line = f.readline()[1:]
            try:
                a = line.split()[0]
            except:
                a = "-1"
                print("invalid a0 range for %s."%folder)
                break
        if a == "-1":
            continue
        while float(a) < a0s[-1]:
            if float(a) in a0s:
                if numvals == 1:
                    ret_dict[folder][a] = line.split()[index]
                else:
                    ret_dict[folder][a] = [line.split()[index[0]],line.split()[index[1]],line.split()[index[2]]]
            line = f.readline()[1:]
            try:
                a = line.split()[0]
            except:
                break
        f.close()
    return ret_dict

def get_one_value(quantity,a0,i):
    if quantity in quantity_dict_single.keys():
        index = quantity_dict_single[quantity]
        numvals = 1
    elif quantity in quantity_dict_triple.keys():
        index = quantity_dict_triple[quantity]
        numvals = 3   
    basepath = "galaxy_catalogs/"
    folder = "VELA_v2_%02i"%i
    if numvals == 1:
        pathname = basepath + folder + "/galaxy_catalogue/Mstar.txt"
    else:
        pathname = basepath + folder + "/galaxy_catalogue/Nir_disc_cat.txt"
    try:
        f = file(pathname)
    except:
        print("Error reading %s"%pathname)
    f.readline()
    a = "0"
    while float(a) < a0:
        line = f.readline()[1:]
        try:
            a = line.split()[0]
        except:
            a = "-1"
            print("invalid a0 range for %s."%folder)
            break
    if numvals == 1:
        return float(line.split()[index])
    else:
        return np.array([float(line.split()[index[0]]),float(line.split()[index[1]]),float(line.split()[index[2]])]


dict_of_vela_info("L",iarr = np.arange(1,35),a0step = 0.05, a0ends = [.2,.551])
