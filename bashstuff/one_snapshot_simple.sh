#!/bin/bash

module load python/2.7.12
virtualenv --system-site-packages ~/myenv
source ~/myenv/bin/activate
python ~/quasarlines/quasarlines/quasar_scan.py 'VELA_v2_08' '~/scratch/10MpcBox_csf512_a0.400.d' "[H I,O VI,O V,O VII]"
deactivate 
