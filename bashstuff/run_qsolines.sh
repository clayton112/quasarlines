#!/bin/bash

#from VELA08 to VELA15
#10MpcBox_csf512_a0.020.d 
#from a0.330 to a0.500

SOURCEDIR=/lou/s2l/dceverin/VELA_v2.0
#SOURCEDIR=testdir
IONS=$'[H I,O VI,O VII,O V]'

for i in $(seq -f "%02g" 08 08)
do
	a=400
	while [[ $a -lt 500 ]]
	do
		bash ~/quasarlines/quasarlines/bashstuff/run_one_snapshot.sh $i $a '$IONS'
		a=$[$a+100]
	done
done