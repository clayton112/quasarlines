#!/bin/bash

#from VELA08 to VELA15
#10MpcBox_csf512_a0.020.d 
#from a0.330 to a0.500

SOURCEDIR=/lou/s2l/dceverin/VELA_v2.0
MINI=06
MAXI=15
MINA=200
MAXA=551
INCREMENT=50


for i in $(seq -f "%02g" $MINI $MAXI)
do
	a=$MINA
	while [[ $a -lt $MAXA ]]
	do
		scp lou:$SOURCEDIR/VELA$i/'10MpcBox_csf512_a0.'$a'.d' ~/scratch
		scp lou:$SOURCEDIR/VELA$i/'PMcrs0a0.'$a'.DAT' ~/scratch
		scp lou:$SOURCEDIR/VELA$i/'PMcrda0.'$a'.DAT' ~/scratch
		a=$[$a+$INCREMENT]
	done
	source ~/myenv/bin/activate
	python ~/quasarlines/one_phase_plot.py 'VELA_v2_'$i
	deactivate 
	cd ~/scratch
	rm *.d
	rm *.DAT
	rm *.h5
	cd ~/quasarlines
	done
done